const getElement = require("../extractor/getElement");
const getExtractor = require("../extractor/getExtractor");

/**
 * Création d'une entité
 * @param {Object} $
 * @param {String} selector
 * @param {String} type
 * @return {String}
 */
async function buildContent($, selector, type, option) {
  // extraction des données
  const content = await getElement($, selector);

  if (option?.direct) {
    // récupération d'un extracteur sinon retourne par défaut le contenu html
    const extractor = type ? getExtractor(type) : getExtractor("html");
    const value = extractor(content);
    if (!value)
      throw {
        code: 404,
        errors: [{ type: `Le type de sélecteur ${type} a échoué` }],
        message: "Not Found",
      };
    return value;
  }

  // accumulateur de valeurs
  const values = [];

  // parse tout les elements répondant au sélecteur
  $(content).each((idx, elt) => {
    // récupération d'un extracteur sinon retourne par défaut le contenu html
    const extractor = type ? getExtractor(type) : getExtractor("html");
    values.push(extractor($(elt)));
  });

  return values;
}

module.exports = buildContent;
