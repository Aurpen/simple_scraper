const extractProperty = require("../extractor/extractProperty");

/**
 * Création d'une entité
 * @param {Object} container
 * @param {Array} properties
 */
async function buildEntity($, properties) {
  let data = {};

  // construction de la donnée à partir de la description
  // dans la requête
  const promises = properties.map((prop) => extractProperty($, prop));
  const values = await Promise.all(promises);

  values.forEach((val) => {
    data = { ...data, ...val };
  });

  return data;
}

module.exports = buildEntity;
