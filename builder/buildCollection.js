const buildEntity = require("./buildEntity");
const getElement = require("../extractor/getElement");

/**
 * Création d'une Collection
 * @param {Function} $
 * @param {String} selector
 * @param {Array} properties
 */
async function buildCollection($, selector, properties) {
  // extraction du container
  const container = await getElement($, selector);

  // Vérification de contenant
  if (container.length < 1) {
    throw { message: "selector of container not found in DOM" };
  }

  const promises = [];

  let itemSelectors;

  // reconstruit les sélecteurs de chaque sous-element
  $(container).each((idx, elt) => {
    const target = $(elt);
    if (target) {
      // réécriture du sélector dans les properties
      itemSelectors = [...properties].map((prop) => ({
        ...prop,
        selector: selector + `:nth-child(${idx + 1}) ` + prop.selector,
      }));
      promises.push(buildEntity($, itemSelectors));
    }
  });

  return await Promise.all(promises);
}

module.exports = buildCollection;
