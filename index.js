"use strict";

module.exports = {
  getCollection: require("./method/getCollection"),
  getContent: require("./method/getContent"),
  getEntity: require("./method/getEntity"),
};
