const getDOM = require("../dom/getDom");
const buildEntity = require("../builder/buildEntity");

/**
 * Création d'une entité
 * @param {Object} options
 * @returns {Object}
 */
async function getEntity({ url, norm, properties }) {
  // récupération du DOM
  const $ = await getDOM(url, norm);

  // construction d'une entité
  return await buildEntity($, properties);
}

module.exports = getEntity;
