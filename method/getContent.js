const getDOM = require("../dom/getDom");
const buildContent = require("../builder/buildContent");

/**
 * Récupération d'un contenu HTML
 * @param {Object} options
 * @return {String}
 */
async function getContent({ url, norm, selector, type }) {
  // récupération du DOM
  const $ = await getDOM(url, norm);

  // extraction des données
  return await buildContent($, selector, type);
}

module.exports = getContent;
