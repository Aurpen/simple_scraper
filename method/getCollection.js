const getDOM = require("../dom/getDom");
const buildCollection = require("../builder/buildCollection");

/**
 * Création d'une liste d'entité
 * @param {Object} options
 * @returns {Object}
 */
async function getCollection({ url, norm, selector, properties }) {
  // récupération du DOM
  const $ = await getDOM(url, norm);

  // construction d'une collection
  return await buildCollection($, selector, properties);
}

module.exports = getCollection;
