const handleErrnoError = require("./handleErrnoError");
const getRequestProtocol = require("./getRequestProtocol");

/**
 * Appel de la page à scrapper
 * @param {String} url
 * @returns {String}
 */
function requestDom(url) {
  // récupération du protocole d'appel correspondant
  const httpCall = getRequestProtocol(url);

  return new Promise((resolve, reject) => {
    // appel de l'url
    const request = httpCall.get(url, (res) => {
      let chunks = [];

      res.on("data", function (chunk) {
        chunks.push(chunk);
      });

      res.on("end", async function () {
        // retour des données
        resolve(Buffer.concat(chunks));
      });
    });

    // gestion erreur NODE
    request.on("error", function (err) {
      const error = {
        ...handleErrnoError(err),
        errors: [{ url: `La requête à ${url} a échoué` }],
        message: `La requête à ${url} a échoué`,
      };
      reject(error);
    });
  });
}

module.exports = requestDom;
