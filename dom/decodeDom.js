const iconv = require("iconv-lite");
/**
 * Decodage du DOM selon norme
 * @param {String} dom
 * @param {String} norm
 * @returns {String}
 */
async function decodeDom(dom, norm = "utf-8") {
  let decodeDom;
  try {
    decodeDom = iconv.decode(dom, norm);
  } catch (err) {
    throw {
      code: 400,
      errors: [{ norm: `le décodage en ${norm} a échoué` }],
      message: " iconv.decode Fail :" + err.message,
    };
  }
  return decodeDom;
}

module.exports = decodeDom;
