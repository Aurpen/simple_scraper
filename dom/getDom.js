const decodeDom = require("./decodeDom");
const extractDom = require("./extractDom");
const requestDom = require("./requestDom");

/**
 * Récupération du DOM
 * @param {String} url
 * @param {String} norm
 * @returns {Function}
 */
async function getDOM(url, norm) {
  // requête du DOM
  const dom = await requestDom(url);

  // décodage du DOM
  const decodedBody = await decodeDom(dom, norm);

  // extraction du DOM
  const $ = await extractDom(decodedBody);
  return $;
}

module.exports = getDOM;
