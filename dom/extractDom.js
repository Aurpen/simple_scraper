const cheerio = require("cheerio");

/**
 * Récupération du DOM
 * @param {String} decodedBody
 * @returns {Function}
 */
async function extractDom(decodedBody) {
  let $;
  try {
    $ = await cheerio.load(decodedBody, {
      decodeEntities: false,
    });
  } catch (err) {
    throw {
      code: 500,
      error: { ...err },
      message: "L'extraction du DOM a échoué",
    };
  }
  return $;
}

module.exports = extractDom;
