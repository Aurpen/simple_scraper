/**
 * Adaptation des erreurs de Node
 * @param {Object} error
 * @return {Object}
 */
function handleErrnoError(error) {
  switch (error.code) {
    case "EPROTO":
      return { code: 495, error, message: "SSL Certificate Error" };
    case "EACCES":
      return { code: 401, error, message: "Unauthorized" };
    case "ERR_HTTP_INVALID_STATUS_CODE":
      return {
        code: 500,
        error,
        message:
          "Status code was outside the regular status code range (100-999)",
      };
    case "ENOTFOUND":
      return { code: 404, error, message: "No Found" };
    default:
      return { code: 500, error, message: "Internal Server Error" };
  }
}

module.exports = handleErrnoError;
