const http = require("http");
const https = require("https");

/**
 * Setter de protocol selon context url ciblée
 * @param {String} url
 * @returns {http | https}
 */
function getRequestProtocol(url) {
  if (/^https:\/\//.test(url)) {
    return https;
  }

  if (/^http:\/\//.test(url)) {
    return http;
  }

  throw {
    code: 500,
    error: { url },
    message: "Can't resolve Protocol for request",
  };
}

module.exports = getRequestProtocol;
