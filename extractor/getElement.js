/**
 * Récupération Impérative d'un element du DOM
 * @param {Function} $
 * @param {String} selector
 */
function getElement($, selector) {
  const content = $(selector);
  if (!content.length) {
    throw {
      code: 404,
      errors: [{ selector: `Le selecteur css ${selector} a échoué` }],
      error: { value: { selector }, message: `Not found element in DOM` },
      message: `Not Found`,
    };
  }
  return content;
}

module.exports = getElement;
