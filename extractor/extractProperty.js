const getElement = require("./getElement");
const getExtractor = require("./getExtractor");

/**
 * Extraction de la valeur dans le Dom
 * @param {Object} $
 * @param {Object} prop
 */
async function extractProperty($, prop) {
  const data = {};
  const { value, selector, nullable } = prop;
  try {
    const elt = await getElement($, selector);
    const extractor = getExtractor(value);
    data[prop.name] = extractor(elt, value);
  } catch (err) {
    data[prop.name] = nullable ? "" : err;
  }
  return Promise.resolve(data);
}

module.exports = extractProperty;
