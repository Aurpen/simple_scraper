/**
 * Récupération de la fonction de récupération de la data par un type
 * @param {String} type
 * @param {Boolean} required
 */
function getExtractor(type) {
  switch (type) {
    case "text":
      return (elt) => elt.text();
    case "html":
      return (elt) => elt.html();
    // par defaut on cherche un attribut
    default:
      return (elt) => elt.attr(type);
  }
}

module.exports = getExtractor;
